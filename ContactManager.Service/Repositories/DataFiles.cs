﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ContactManager.Service.Models;
using Newtonsoft.Json;

namespace ContactManager.Service.Repositories
{
    public class DataFiles : IRepository
    {
        private readonly string fileLocation;
        public DataFiles()
        {
            this.fileLocation = ConfigurationManager.AppSettings[Resource.DataFilesLocation];
        }

        public IEnumerable<ContactData> GetContactData()
        {
            List<ContactData> contactData = new List<ContactData>();
             
            foreach (string file in GetFileList())
            {
                var contact = JsonConvert.DeserializeObject<ContactData>(File.ReadAllText(file));
                contactData.Add(contact);
            }

            return contactData;
        }

        public void UpdateContact(ContactData contactData)
        {
            var dataToSave = JsonConvert.SerializeObject(contactData);
            var fileName = $"{contactData.ContactId}.json";
            WriteContactData(dataToSave, fileName);
        }

        private string[] GetFileList()
        {
            
            return Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), this.fileLocation));
        }

        private void WriteContactData(string data, string fileName)
        {
            var filePath = $"{Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))}\\{this.fileLocation}\\{fileName}";
            File.WriteAllText(filePath, data);
        }
    }
}
