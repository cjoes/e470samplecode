﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager.Service.Models
{
    public class Contact 
    {
        private Contact() { }
        
        public Contact(string ContactId, string firstName, string lastName, string address, string city, string state, string zip)
        {
            this.ContactId = ContactId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address = address;
            this.City = city;
            this.State = state;
            this.Zip = zip;
        }

        [JsonProperty]
        public string ContactId { get; private set; }
        [JsonProperty]
        public String FirstName { get; private set; }
        [JsonProperty]
        public String LastName { get; private set; }
        [JsonProperty]
        public String Address { get; private set; }
        [JsonProperty]
        public String City { get; private set; }
        [JsonProperty]
        public String State { get; private set; }
        [JsonProperty]
        public String Zip { get; private set; }
    }
}
