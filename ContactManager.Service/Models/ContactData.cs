﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager.Service.Models
{
    public class ContactData
    {

        public ContactData(string contactId, string firstName, string lastName, string address, string city, string state, string zip, string internalData1, string internalData2, string internalData3)
        {
            this.ContactId = contactId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address = address;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.InternalData1 = internalData1;
            this.InternalData2 = internalData2;
            this.InternalData3 = internalData3;
        }

        private ContactData() { }

        public string ContactId { get; private set; }
        [JsonProperty]
        public string FirstName { get; private set; }
        [JsonProperty]
        public string LastName { get; private set; }
        [JsonProperty]
        public string Address { get; private set; }
        [JsonProperty]
        public string City { get; private set; }
        [JsonProperty]
        public string State { get; private set; }
        [JsonProperty]
        public string Zip { get; private set; }
        [JsonProperty]
        public string InternalData1 { get; private set; }
        [JsonProperty]
        public string InternalData2 { get; private set; }
        [JsonProperty]
        public string InternalData3 { get; private set; }
    }
}
