﻿using ContactManager.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager.Service.Controllers
{
    public interface IContactsController
    {
        IEnumerable<Contact> Get();

        void Put(Contact contact);
    }
}
